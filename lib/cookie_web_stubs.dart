import 'cookie.dart';

// A duplicate file that doesn't import dart:html
// These files shouldn't ever be accessed on any platform
// They are only used to satisfy the analyzer at build time
class CookieHandlerWeb implements Cookie {
  @override
  String formatDate(DateTime date) {
    throw UnimplementedError();
  }

  @override
  String? get(String key) {
    throw UnimplementedError();
  }

  @override
  bool remove(String key, {String? path, String? domain, bool? secure}) {
    throw UnimplementedError();
  }

  @override
  void set(String key, String value,
      {DateTime? expires,
      Duration? maxAge,
      String? path,
      String? domain,
      bool? secure}) {
    throw UnimplementedError();
  }
}
